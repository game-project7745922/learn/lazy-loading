using System;
using Color = UnityEngine.Color;

namespace DickyArya.LazyLoading.Data
{
    [Serializable]
    public class DataDummy
    {
        public string CardName;
        public Color CardColor;
        public string CardDescription;

        public DataDummy(string cardName)
        {
            CardName = cardName;
            CardColor = new Color(UnityEngine.Random.Range(0, 1f), UnityEngine.Random.Range(0, 1f), UnityEngine.Random.Range(0, 1f));
            CardDescription = $"Berikut adalah deskripsi untuk {cardName}";
        }
    }
}
