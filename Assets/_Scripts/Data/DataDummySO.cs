using UnityEngine;

namespace DickyArya.LazyLoading.Data
{
    [CreateAssetMenu(fileName = "DataDummy", menuName = "Lazy loading/DataDummy")]
    public class DataDummySO : ScriptableObject
    {
        public DataDummy[] Datas;

        private string[] firstNames = { "Alex", "Chris", "Jordan", "Taylor", "Morgan", "Jamie", "Casey", "Avery" };
        private string[] middleNames = { "Lee", "Sky", "Ray", "Jay", "Drew", "Max", "Kai", "Blake" };
        private string[] lastNames = { "Smith", "Johnson", "Brown", "Taylor", "Anderson", "Thomas", "Jackson", "White" };

        public string GenerateRandomName()
        {
            string firstName = firstNames[Random.Range(0, firstNames.Length)];
            string middleName = middleNames[Random.Range(0, middleNames.Length)];
            string lastName = lastNames[Random.Range(0, lastNames.Length)];

            return $"{firstName} {middleName} {lastName}";
        }

        private void OnValidate()
        {
            if (Datas.Length > 0)
            {
                Datas[Datas.Length - 1] = new DataDummy(GenerateRandomName());
            }
        }
    }
}
