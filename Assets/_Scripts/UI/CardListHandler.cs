using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using DickyArya.LazyLoading.Data;
using UnityEngine;
using UnityEngine.UI;

namespace DickyArya.LazyLoading.UI
{
    public class CardListHandler : MonoBehaviour
    {
        internal enum LoadType { Task, Coroutine }

        [Header("Setup")]
        [SerializeField] DataDummySO _dataDummySO;
        [SerializeField] CardUI _cardUI;
        [SerializeField] ScrollRect _scrollRect;
        [SerializeField] Image _loadingProgressImage;

        [Header("Debug")]
        [SerializeField] LoadType _loadType;

        int _itemPerBatch = 3;
        int _itemLoaded = 0;
        int _totalItems = 0;

        float _waitToLoadTime = 10f;

        bool _isLoading = false;

        private List<CardUI> _cards = new List<CardUI>();

        private void Start()
        {
            _loadingProgressImage.gameObject.SetActive(false);
            _scrollRect.onValueChanged.AddListener(OnScroll);
            _totalItems += _itemPerBatch;

            LoadCardHandler();
        }

        private void OnScroll(Vector2 pos)
        {
            if (_totalItems == _dataDummySO.Datas.Length) return;

            if (_scrollRect.verticalNormalizedPosition <= .001f && !_isLoading)
            {
                Debug.Log("Trigger Load");
                if (_dataDummySO.Datas.Length > _totalItems)
                    _totalItems += (_dataDummySO.Datas.Length - _totalItems >= _itemPerBatch) ? _itemPerBatch : _dataDummySO.Datas.Length - _totalItems;

                LoadCardHandler();
            }

        }

        void LoadCardHandler()
        {
            switch (_loadType)
            {
                case LoadType.Task:
                    LoadCard();
                    break;
                case LoadType.Coroutine:
                    StartCoroutine(LoadCardCO());
                    break;
            }
        }

        private async UniTask SetCardTask(DataDummy data)
        {
            var card = SpawnCard(data);
            _itemLoaded++;
            _cards.Add(card);
            await UniTask.Delay(1000);
        }

        private async void LoadCard()
        {
            Debug.Log("Start Load");

            _isLoading = true;

            _loadingProgressImage.gameObject.SetActive(true);

            var tween = DOVirtual.Float(0, 1f, .5f, UpdateFillImage).SetLoops(-1, LoopType.Restart);
            int itemLoadedDifference = 0;
            int currentItemLoaded = _itemLoaded;

            while (_itemLoaded < _totalItems)
            {
                var loadCardTask = new UniTask[_totalItems - _itemLoaded];

                for (int i = _itemLoaded; i < _totalItems; i++)
                {
                    var data = _dataDummySO.Datas[i];

                    loadCardTask[i - currentItemLoaded] = SetCardTask(data);

                    itemLoadedDifference++;
                }

                await UniTask.WhenAll(loadCardTask);
            }

            Debug.Log("Done Load");

            for (int i = _itemLoaded - itemLoadedDifference; i < _totalItems; i++)
                _cards[i].gameObject.SetActive(true);

            _loadingProgressImage.gameObject.SetActive(false);
            tween.Kill(true);

            _isLoading = false;
        }

        public void SetCard(DataDummy data)
        {
            var card = SpawnCard(data);
            _itemLoaded++;
            _cards.Add(card);
        }

        private IEnumerator LoadCardCO()
        {
            _isLoading = true;

            _loadingProgressImage.gameObject.SetActive(true);

            var tween = DOVirtual.Float(0, 1f, .6f, UpdateFillImage).SetLoops(-1, LoopType.Restart);
            int itemLoadedDifference = 0;

            while (_itemLoaded < _totalItems)
            {
                for (int i = _itemLoaded; i < _totalItems; i++)
                {
                    var data = _dataDummySO.Datas[i];
                    SetCard(data);
                    itemLoadedDifference++;
                }
                yield return new WaitForSeconds(_waitToLoadTime);
            }

            for (int i = _itemLoaded - itemLoadedDifference; i < _totalItems; i++)
                _cards[i].gameObject.SetActive(true);

            _loadingProgressImage.gameObject.SetActive(false);
            tween.Kill(true);

            _isLoading = false;
        }

        private CardUI SpawnCard(DataDummy data)
        {
            var card = Instantiate(_cardUI, transform);
            card.SetCard(data.CardColor, data.CardName, data.CardDescription);
            card.gameObject.SetActive(false);

            return card;
        }

        private void UpdateFillImage(float value)
        {
            _loadingProgressImage.fillAmount = value;
        }
    }

}
