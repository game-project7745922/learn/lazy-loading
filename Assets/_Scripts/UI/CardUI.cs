using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DickyArya.LazyLoading.UI
{
    public class CardUI : MonoBehaviour
    {
        [SerializeField] Image _cardImage;
        [SerializeField] TextMeshProUGUI _nameText;
        [SerializeField] TextMeshProUGUI _descText;

        public void SetCard(Color color, string name, string desc)
        {
            _cardImage.color = color;
            _nameText.text = name;
            _descText.text = desc;
        }
    }
}
